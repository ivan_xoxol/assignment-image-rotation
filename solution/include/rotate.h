#ifndef ROTATE_H
#define ROTATE_H

#include "status.h"
#include <stdio.h>

struct image rotate( struct image const source );

#endif
