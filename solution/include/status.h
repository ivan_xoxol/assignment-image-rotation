#ifndef STATUS_H
#define STATUS_H

#include <stdio.h>
#include <stdlib.h>

enum main_status {
    MAIN_OK = 0,
    MAIN_ARG_ERROR,
    MAIN_OPEN_ERROR,
    MAIN_CLOSE_ERROR,
    MAIN_READ_ERROR,
    MAIN_WRITE_ERROR,
    MAIN_ROTATE_ERROR,
    MAIN_FREE_ERROR,
};

enum open_status {
    OPEN_OK = 0,
    OPEN_NULL_ERROR,
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_NULL_ERROR,
};

enum read_status {
    READ_OK = 0,
    READ_MALLOC_NULL,
    READ_END_OF_FILE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PLANES,
    READ_ERROR,
    READ_PIXEL_ERROR,
    READ_SEEK_ERROR,
};

enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_PIXEL_ERROR,
    WRITE_PADDING_ERROR,
};

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_MALLOC_NULL,
    ROTATE_ERROR,
};

enum free_status {
    FREE_OK = 0,
    FREE_ERROR,
};

#endif
