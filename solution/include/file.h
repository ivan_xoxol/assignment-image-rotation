#ifndef OPEN_H
#define OPEN_H

#include "status.h"
#include <stdio.h>

enum open_status file_open( FILE** file_ptr, const char* path, const char* mode );

enum close_status file_close( FILE** file_ptr );

#endif
