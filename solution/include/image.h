#ifndef IMAGE_H
#define IMAGE_H

#include "status.h"
#include <stdint.h>
#include <stdio.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum free_status free_heap( struct image* source );

#endif
