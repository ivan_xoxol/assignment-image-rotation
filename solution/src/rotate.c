#include "bmp.h"
#include "image.h"
#include "status.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static enum rotate_status try_rotate( struct image const* source, struct image* rotated );
static void do_rotate( struct image const* source, struct image* rotated );

struct image rotate( struct image const source ) {
    struct image rotated = {
            .width = source.height,
            .height = source.width,
            .data = NULL,
    };
    enum rotate_status try_rotate_status = try_rotate( &source, &rotated );
    if ( try_rotate_status == ROTATE_OK ) return rotated;
    return rotated;
}

static enum rotate_status try_rotate( struct image const* source, struct image* rotated ) {
    struct pixel* data_pointer = malloc( (size_t)source->height *
                                         (size_t)source->width *
                                            sizeof(struct pixel) );
    if ( data_pointer == NULL ) return ROTATE_MALLOC_NULL;
    rotated->data = data_pointer;
    do_rotate( source, rotated );
    return ROTATE_OK;
}

static void do_rotate( struct image const* source, struct image* rotated ) {
    for( uint64_t i = 0; i < rotated->width; i++ )
        for( uint64_t j = 0; j < rotated->height; j++ )
            rotated->data[j * source->height + source->height - 1 - i] = source->data[i * source->width + j];
}
