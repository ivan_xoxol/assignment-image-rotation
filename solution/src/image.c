#include "image.h"
#include "status.h"
#include <stdio.h>
#include <stdlib.h>

enum free_status free_heap( struct image* source ) {
    if ( source != NULL ) {
        free( source->data );
        return FREE_OK;
    }
    return FREE_ERROR;
}
