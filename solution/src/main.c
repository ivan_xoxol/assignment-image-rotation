#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"
#include "status.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if ( argc != 3 ) {
        return MAIN_ARG_ERROR;
    }

    FILE *file_input = NULL;
    FILE *file_output = NULL;

    enum open_status file_input_open_status = file_open( &file_input, argv[1], "rb");
    enum open_status file_output_open_status = file_open( &file_output, argv[2], "wb");
    if ( file_input_open_status == OPEN_NULL_ERROR || 
            file_output_open_status == OPEN_NULL_ERROR ) {
                return MAIN_OPEN_ERROR;
    }

    struct image image;
    enum read_status file_input_read_status = from_bmp( file_input, &image );
    if ( file_input_read_status != READ_OK ) { return MAIN_READ_ERROR; }

    enum close_status file_input_close_status = file_close( &file_input );
    if ( file_input_close_status == CLOSE_NULL_ERROR ) { return MAIN_CLOSE_ERROR; }

    struct image rotated = rotate( image );
    if ( rotated.data == NULL ) { return MAIN_ROTATE_ERROR; }

    enum free_status free_image_heap_status = free_heap( &image );
    if ( free_image_heap_status == FREE_ERROR ) { return MAIN_FREE_ERROR; }

    enum write_status file_output_write_status = to_bmp( file_output, &rotated );
    if ( file_output_write_status != WRITE_OK ) { return MAIN_WRITE_ERROR; }

    enum close_status file_output_close_status = file_close( &file_output );
    if ( file_output_close_status == CLOSE_NULL_ERROR ) { return MAIN_CLOSE_ERROR; }
    
    enum free_status free_rotated_heap_status = free_heap( &rotated );
    if ( free_rotated_heap_status == FREE_ERROR ) { return MAIN_FREE_ERROR; }

    return MAIN_OK;
}
