#include "bmp.h"
#include "status.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define PIXELBSIZE 3
#define DELVALUE 4
#define READCOUNT 1
#define WRITECOUNT 1

#define BFTIPE 0x4D42
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIPLANES 1
#define BFRESERVED 0
#define BOFFBITS 54
#define BISIZE 40
#define BIXPELSPERMETER 0
#define BIYPELSPERMETER 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0

static uint8_t get_padding( uint64_t value ) { 
    return (DELVALUE - (value * PIXELBSIZE) % DELVALUE) % DELVALUE; 
}

static struct bmp_header make_bmp_output_header( uint8_t padding, 
                                                    uint32_t width, 
                                                    uint32_t height ) {
    return (struct bmp_header) {
        .bfType = BFTIPE,
        .biBitCount = BIBITCOUNT,
        .biCompression = BICOMPRESSION,
        .biPlanes = BIPLANES,
        .bfReserved = BFRESERVED,
        .biWidth = width,
        .biHeight = height,
        .biSizeImage = height * (width * PIXELBSIZE + padding),
        .bfileSize = BOFFBITS + (width * PIXELBSIZE + padding) * height,
        .bOffBits = BOFFBITS,
        .biSize = BISIZE,
        .biXPelsPerMeter = BIXPELSPERMETER,
        .biYPelsPerMeter = BIYPELSPERMETER,
        .biClrUsed = BICLRUSED,
        .biClrImportant = BICLRIMPORTANT
    };
}

static enum read_status read_row( uint64_t row, uint64_t width, uint8_t padding, 
                                                                FILE* in, struct image* img ) {
    for ( uint64_t px = 0; px < width; px++ ) {
        size_t fread_pixel_status = fread( &(img->data[row * width + px]), sizeof(struct pixel), READCOUNT, in );
        if ( fread_pixel_status != READCOUNT && feof(in) == 0 && ferror(in) ) return READ_PIXEL_ERROR; 
    }
    int seek_status = fseek( in, padding, 1 );
    if ( seek_status != 0 ) return READ_SEEK_ERROR;
    return READ_OK;
}

static enum read_status read_pixels( struct bmp_header from_bmp_header, FILE* in, struct image* img ) {
    uint64_t height = (uint64_t) from_bmp_header.biHeight;
    uint64_t width = (uint64_t) from_bmp_header.biWidth;
    uint8_t padding = get_padding( width );
    img->height = height;
    img->width = width;
    struct pixel* data_pointer = malloc( (size_t)height * (size_t)width * sizeof(struct pixel) );
    if ( data_pointer == NULL ) return READ_MALLOC_NULL;
    img->data = data_pointer;
    for ( uint64_t i = 0; i < height; i++ ) {
        enum read_status read_row_status = read_row( i, width, padding, in, img );
        if ( read_row_status != READ_OK ) return read_row_status;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header from_bmp_header;
    size_t fread_header_status = fread( &from_bmp_header, sizeof(from_bmp_header), READCOUNT, in );
    if ( fread_header_status == READCOUNT ) {
        enum read_status read_pixels_status = read_pixels( from_bmp_header, in, img );
        if ( read_pixels_status != READ_OK ) return read_pixels_status;
    }
    else if ( feof(in) )                                            { return READ_END_OF_FILE; }
    else if ( ferror(in) ) {
        if      ( from_bmp_header.bfType != BFTIPE )                { return READ_INVALID_SIGNATURE; }
        else if ( from_bmp_header.biBitCount != BIBITCOUNT )        { return READ_INVALID_BITS; }
        else if ( from_bmp_header.biCompression != BICOMPRESSION )  { return READ_INVALID_HEADER; }
        else if ( from_bmp_header.biPlanes != BIPLANES )            { return READ_INVALID_PLANES; }
        else                                                        { return READ_ERROR; }
    }
    return READ_OK;
}

static enum write_status write_row( uint64_t row, uint64_t width, uint8_t padding,
                                    FILE* out, struct image const* img ) {
    for ( uint64_t px = 0; px < width; px++ ) {
            size_t fwrite_pixel_status = fwrite( &img->data[row * width + px], sizeof(struct pixel), WRITECOUNT, out );
            if ( fwrite_pixel_status != WRITECOUNT ) return WRITE_PIXEL_ERROR;
    }
    for ( uint8_t pd = 0; pd < padding; pd++ ) {
        uint8_t pad_val = 0;
        size_t fwrite_padding_status = fwrite( &pad_val, sizeof(uint8_t), WRITECOUNT, out );
        if ( fwrite_padding_status != WRITECOUNT ) return WRITE_PADDING_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    uint64_t height = img->height;
    uint64_t width = img->width;
    uint8_t padding = get_padding( width );
    struct bmp_header to_bmp_header = make_bmp_output_header( padding, (uint32_t)width, (uint32_t)height );
    size_t fwrite_header_status = fwrite( &to_bmp_header, sizeof(to_bmp_header), WRITECOUNT, out );
    if ( fwrite_header_status != WRITECOUNT ) return WRITE_HEADER_ERROR;
    for ( uint64_t i = 0; i < height; i++ ) {
        enum write_status write_row_status = write_row( i, width, padding, out, img );
        if ( write_row_status != WRITE_OK ) return write_row_status;
    }
    return WRITE_OK;
}
