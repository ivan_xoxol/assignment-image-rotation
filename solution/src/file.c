#include "file.h"
#include "status.h"
#include <stdio.h>

enum open_status file_open( FILE** file_ptr, const char* path, const char* mode ) {
    *file_ptr = fopen( path, mode );
    if ( *file_ptr == NULL) return OPEN_NULL_ERROR;
    return OPEN_OK;
}

enum close_status file_close( FILE** file_ptr ) {
    if ( *file_ptr == NULL ) return CLOSE_NULL_ERROR;
    fclose( *file_ptr );
    return CLOSE_OK;
}
